from model import Glow
import torch
from math import log, sqrt, pi
from tqdm import tqdm
from torch import nn, optim
from torch.autograd import Variable, grad
from torch.utils.data import DataLoader
from torchvision import datasets, transforms
import argparse

device = torch.device("cuda:1" if torch.cuda.is_available() else "cpu")
parser = argparse.ArgumentParser(description="Glow infer")
parser.add_argument("--batch", default=128, type=int, help="batch size")
parser.add_argument(
    "--n_flow", default=32, type=int, help="number of flows in each block"#32
)
parser.add_argument("--n_block", default=3, type=int, help="number of blocks")
parser.add_argument(
    "--no_lu",
    action="store_true",
    help="use plain convolution instead of LU decomposed version",
)
parser.add_argument(
    "--affine", action="store_true", help="use affine coupling instead of additive"
)
parser.add_argument("--n_bits", default=5, type=int, help="number of bits")
parser.add_argument("--img_size", default=32, type=int, help="image size")
parser.add_argument("--temp", default=0.75, type=float, help="temperature of sampling")
parser.add_argument("--n_sample", default=20, type=int, help="number of samples")
parser.add_argument("path", metavar="PATH", type=str, help="Path to image directory")
parser.add_argument("--iter", default=2, type=int, help="maximum iterations")


def sample_data(path, batch_size, image_size):
    transform = transforms.Compose(
        [
            transforms.Resize(image_size),
            transforms.CenterCrop(image_size),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
        ]
    )

    dataset = datasets.ImageFolder(path, transform=transform)
    loader = DataLoader(dataset, shuffle=True, batch_size=batch_size, num_workers=4)
    loader = iter(loader)

    while True:
        try:
            yield next(loader)

        except StopIteration:
            loader = DataLoader(
                dataset, shuffle=True, batch_size=batch_size, num_workers=4
            )
            loader = iter(loader)
            yield next(loader)

def calc_z_shapes(n_channel, input_size, n_flow, n_block):
    z_shapes = []
    for i in range(n_block - 1):
        input_size //= 2
        n_channel *= 2

        z_shapes.append((n_channel, input_size, input_size))

    input_size //= 2
    z_shapes.append((n_channel * 4, input_size, input_size))

    return z_shapes

def calc_loss(log_p, logdet, image_size, n_bins):
    # log_p = calc_log_p([z_list])
    n_pixel = image_size * image_size * 3

    loss = -log(n_bins) * n_pixel
    loss = loss + logdet + log_p

    return (
        (-loss / (log(2) * n_pixel)).mean(),
        (log_p / (log(2) * n_pixel)).mean(),
        (logdet / (log(2) * n_pixel)).mean(),
    )

def infer(args, model):
    dataset = iter(sample_data(args.path, args.batch, args.img_size))
    n_bins = 2.0 ** args.n_bits

    # z_sample = []
    # z_shapes = calc_z_shapes(3, args.img_size, args.n_flow, args.n_block)
    # print(len(z_shapes))
    # for z in z_shapes:
    #     z_new = torch.randn(args.n_sample, *z) * args.temp
    #     z_sample.append(z_new.to(device))
    for _ in tqdm(range(0,20)):
        torch.cuda.empty_cache() 
        image, _ = next(dataset)
        image = image.to("cuda:1")
        image = image * 255

        if args.n_bits < 8:
            image = torch.floor(image / 2 ** (8 - args.n_bits))

        image = image / n_bins - 0.5
        print(image.shape[0])
        with torch.no_grad():
            log_p, logdet, _ = model.module(image + torch.rand_like(image) / n_bins)
            logdet = logdet.mean()
        loss, log_p, log_det = calc_loss(log_p, logdet, args.img_size, n_bins)
    return loss,log_p,log_det

if __name__ == "__main__":
    args = parser.parse_args()
    print(args)
    model_single = Glow(
            3, args.n_flow, args.n_block, affine=args.affine, conv_lu=not args.no_lu
        )
    model = nn.DataParallel(model_single,device_ids = [1])
    model = model.to(device)
    PATH = "/home/royd1990/workspace/advanceddeeplearningprojectkth/checkpoint/model_000500.pt"
    checkpoint = torch.load(PATH,map_location={'cuda:1':'cuda:0'})#

    # print(new_state_dict.keys())
    model.load_state_dict(checkpoint)#model
    model = model.to(f'cuda:{model.device_ids[0]}')
    #model = model.to(device)
    print(model.output_device)
    loss,logp,logdet = infer(args,model)
    print(loss,logp,logdet)
